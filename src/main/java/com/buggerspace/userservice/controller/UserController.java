package com.buggerspace.userservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.buggerspace.userservice.dao.UserRepository;
import com.buggerspace.userservice.entities.User;

import javassist.NotFoundException;

@RestController
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/users")
	public List<User> getUsers(){
		
		return userRepository.findAll();
	}
	
	@PostMapping("/user")
	public User addUser(@RequestBody User user) {
		
		userRepository.save(user);
		return user;
	}
	
	@GetMapping("/users/{id}")
	public Optional<User> getUserById(@PathVariable("id") Long id){
		
		return userRepository.findById(id);
	}
	 
	@DeleteMapping("/users/{id}")
	public String deleteUser(@PathVariable("id") Long id) throws  NotFoundException{
		
		User user = userRepository.findById(id).orElseThrow(() ->new NotFoundException("User not found on : " + id));
		userRepository.delete(user);
		return "User Deleted";
	}
	
	@PutMapping("/users/{id}")
	public User updateUser(@RequestBody User user){
		userRepository.save(user);
		return user;
	}
	
	
}
