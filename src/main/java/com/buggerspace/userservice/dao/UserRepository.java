package com.buggerspace.userservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.buggerspace.userservice.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

}
